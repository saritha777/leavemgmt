import { SwaggerConfig } from './swagger.interface';

/**
 * swagger config object
 */
export const SWAGGER_CONFIG: SwaggerConfig = {
  title: 'Leave management',
  description: 'Templete',
  version: '1.0',
  tags: ['Templete']
};
