import {
  BadRequestException,
  Injectable,
  Logger,
  Res,
  UnauthorizedException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository } from 'typeorm';
import { UserLeave } from './entity/leave.entity';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/user.entity';
import { Response, Request } from 'express';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
/**
 * user service having all the business logics
 */
@Injectable()
export class UserService {
  /**
   * creating logger object
   */
  logger: Logger;
  constructor(
    /**
     * creating leave repository
     */
    @InjectRepository(UserLeave)
    private leaveRepository: Repository<UserLeave>,
    /**
     * creating user repository
     */
    @InjectRepository(User)
    private userRepository: Repository<User>,
    /**
     * injecting jwt service
     */
    private jwtService: JwtService
  ) {
    this.logger = new Logger(UserService.name);
  }
  /**
   * method for getting all user details
   * @returns all user details
   */
  async getUsers(): Promise<User[]> {
    this.logger.log('Getting all users');
    return await this.userRepository.find();
  }

  /**
   * method for getting leave details by id
   * @param id taking user id as input
   * @returns leave details
   */
  async getLeavesById(id: number): Promise<UserLeave> {
    this.logger.log('Getting leaves with id');
    return await this.leaveRepository.findOne(id);
  }
  /**
   * method for getting user data based on leave date
   * @param leavedate taking leave date as input
   * @returns user data based on leave date
   */
  async getLeaveByLeavedate(leavedate: string): Promise<User[]> {
    this.logger.log('Getting leaves with leave date');
    const leavedetails = await this.userRepository.find({
      leaveDate: leavedate
    });
    return leavedetails;
  }

  /**
   * method for applying leave
   * @param newData taking user data as input
   * @returns string applied for the leave or not
   */
  async applyLeave(newData: User): Promise<string> {
    const leaveData = new UserLeave();
    leaveData.totalLeaves = 15;
    leaveData.leavesTaken = newData.noOfDays;
    leaveData.availableLeaves = leaveData.totalLeaves - leaveData.leavesTaken;
    //   this.leaveRepository.save(leaveData);

    const userData = new User();
    const login = new UserLogin();
    const pass = await bcrypt.hash(newData.password, 10);
    userData.name = login.name = newData.name;
    userData.password = login.password = pass;
    userData.leaveDate = newData.leaveDate;
    userData.noOfDays = newData.noOfDays;
    userData.addLeave(leaveData);
    userData.login = login;

    this.userRepository.save(userData);
    return `successfully applied leave for ${newData.noOfDays}`;
  }

  /**
   * checking user is logined or not
   * @param data taking data as input
   * @param response taking response
   * @returns string the user loged in or not
   */
  async loginUser(data: UserLogin, @Res() response: Response) {
    const user = await this.userRepository.findOne({ name: data.name });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('pasward incorrect');
    }
    const jwt = await this.jwtService.signAsync({ id: user.userId });
    response.cookie('jwt', jwt, { httpOnly: true });
    return {
      message: 'success'
    };
  }

  /**
   * method for finding the user
   * @param request taking request
   * @returns user data
   */
  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];

      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.userRepository.findOne({ userId: data.id });
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }
  /**
   * method for update
   * @param id taking user id as input
   * @param data taking user data for updating
   * @returns string whether the data is updated or not
   */
  updateUser(id: number, data: User) {
    // this.userRepository.update({userId:id},user)
    // return 'success';
    const user = new User();
    user.name = data.name;
    user.leaveDate = data.leaveDate;
    user.noOfDays = data.noOfDays;

    this.userRepository.update({ userId: id }, user);

    const leave = new UserLeave();
    const total = (leave.totalLeaves = 15);
    leave.leavesTaken = user.noOfDays;
    leave.availableLeaves = total - leave.leavesTaken;

    this.leaveRepository.update({ id: id }, leave);
    return 'successfully updated';
  }

  /**
   * method for deleting user
   * @param id taking user id as input
   * @returns data is deleted or not
   */
  async deleteUser(id: number) {
    await this.userRepository.delete(id);
    return this.logger.log('delete user with id');
    //  return 'Deleted leave data succesfully';
  }

  /**
   * logout method for the user
   * @param response taking response
   * @returns string
   */
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout'
    };
  }
}
