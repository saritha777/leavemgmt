import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { User } from './user.entity';

/**
 * userLogin entity
 */
@Entity()
export class UserLogin {
  /**
   * id as primary generated column
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * name property
   */
  @ApiProperty()
  @Column()
  name: string;

  /**
   * password property
   */
  @ApiProperty()
  @Column()
  password: string;

  /**
   * one to one with login
   */
  @OneToOne(() => User, (user) => user.login, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  @JoinColumn()
  user: User;
}
