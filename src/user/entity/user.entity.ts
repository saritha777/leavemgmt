import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { UserLeave } from './leave.entity';
import { UserLogin } from './login.entity';

/**
 * user entity
 */
@Entity()
export class User {
  /**
   * user id parameter
   */
  @PrimaryGeneratedColumn()
  userId: number;

  /**
   * name property with unique valu
   */
  @ApiProperty()
  @IsString()
  @Column({ unique: true })
  name: string;

  /**
   * password parameter
   */
  @ApiProperty()
  @IsString()
  @Column()
  password: string;

  /**
   * leave date parameter
   */
  @ApiProperty()
  @IsString()
  @Column()
  leaveDate: string;

  /**
   * no of days parameter
   */
  @ApiProperty()
  @IsInt()
  @Column()
  noOfDays: number;

  /**
   * one to one relation with login entity
   */
  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true
  })
  login: UserLogin;

  /**
   * one to many relation with leave entity
   */
  @OneToMany(() => UserLeave, (leave) => leave.user, {
    cascade: true
  })
  @JoinColumn()
  leaves: UserLeave[];

  /**
   * method for add leave
   * @param leave taking leave data
   */
  addLeave(leave: UserLeave) {
    if (this.leaves == null) {
      this.leaves = new Array<UserLeave>();
    }
    this.leaves.push(leave);
  }
}
