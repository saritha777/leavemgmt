import { ApiProperty } from '@nestjs/swagger';
import { IsInt } from 'class-validator';
import { type } from 'os';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { User } from './user.entity';

/**
 * user leave entity
 */
@Entity()
export class UserLeave {
  /**
   * id as primary generated column
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * available leaves property
   */
  @ApiProperty()
  @IsInt()
  @Column()
  availableLeaves: number;

  /**
   * leaves taken property
   */
  @ApiProperty()
  @IsInt()
  @Column()
  leavesTaken: number;

  /**
   * total leaves property
   */
  @Column()
  totalLeaves: number;

  /**
   * many to one with user
   */
  @ManyToOne(() => User, (user) => user.leaves, {
    onDelete: 'CASCADE'
  })
  user: User;
}
