import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserLeave } from './entity/leave.entity';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserLeave, UserLogin]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' }
    })
  ],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
