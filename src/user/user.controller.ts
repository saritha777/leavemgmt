import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  Res
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UserLeave } from './entity/leave.entity';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/user.entity';
import { UserService } from './user.service';
import { Request, Response } from 'express';
import { customException } from '../customException/custom.exception';

/**
 * for the tag
 */
@ApiTags('users')
@Controller('user')
/**
 * user controller for checking user data
 */
export class UserController {
  /**
   *
   * @param userService injecting user service for business logic
   */
  constructor(private readonly userService: UserService) {}

  /**
   * method to apply for a leave
   * @param data is user data
   * @returns string whether the user is applied for a leave are not
   */
  @Post()
  applyLeave(@Body() data: User): Promise<string> {
    return this.userService.applyLeave(data);
  }

  // checking user is logined or not
  /**
   * method for checking user is logedin or not
   * @param login taking user login data
   * @param response gettIng the response
   * @returns sucess when the user is loged in otherwise return bad request
   */
  @Post('/login')
  async login(
    @Body() login: UserLogin,
    @Res({ passthrough: true }) response: Response
  ) {
    return this.userService.loginUser(login, response);
  }

  //find user using cookie
  /**
   * method for finding user based on cookie
   * @param request requesting cookie
   * @returns
   */
  @Get('/userlogin')
  async user(@Req() request: Request) {
    return this.userService.findUser(request);
  }

  // logout user
  /**
   * method for logout
   * @param response taking response
   * @returns string when user is logout
   */
  @Post('/logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    return this.userService.logout(response);
  }

  /**
   * method for getting user data
   * @returns all users in the repository
   */
  @Get()
  getUsers(): Promise<User[]> {
    return this.userService
      .getUsers()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no users, please add one user',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'no users, please add one user',
          HttpStatus.NOT_FOUND
        );
      });
  }

  /**
   * method for leave details
   * @param id taking user id as parameter
   * @returns leave details
   */
  @Get('/:id')
  getLeavesData(@Param('id') id: number): Promise<UserLeave> {
    return this.userService
      .getLeavesById(id)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException('user id not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new HttpException('user id not found', HttpStatus.NOT_FOUND);
      });
  }

  /**
   * method for getting user details based on leave date
   * @param leavedate taking leave data as parameter
   * @returns all the users based on leave date
   */
  @Get('leavedata/:leavedate')
  getByDate(@Param('leavedate') leavedate: string): Promise<User[]> {
    return this.userService
      .getLeaveByLeavedate(leavedate)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no users found for the leave date',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'no users found for the leave date',
          HttpStatus.NOT_FOUND
        );
      });
  }

  /**
   * method for updating the user data
   * @param id taking user id as input
   * @param user user data as input for updating
   * @returns string whether the deatails are updated or not
   */
  @Put('user/:id')
  updateUser(@Param('id') id: number, @Body() user: User) {
    return this.userService.updateUser(id, user);
  }

  /**
   * method for deleting user details
   * @param id taking user id as input
   * @returns user is deleted or not
   */
  @Delete('/:id')
  delete(@Param('id') id: number) {
    return this.userService.deleteUser(id);
  }
}
