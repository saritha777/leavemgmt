import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

/**
 * logger middleware class
 */
@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  private logger = new Logger('HTTP');

  /**
   *
   * @param request taking request
   * @param response taking response
   * @param next method for exectuting next process
   */
  use(request: Request, response: Response, next: NextFunction): void {
    const { ip, method, originalUrl } = request;
    /**
     * creating userAgent object
     */
    const userAgent = request.get('user-agent') || '';

    response.on('finish', () => {
      const { statusCode } = response;
      const contentLength = response.get('content-length');

      this.logger.log(
        `${method} ${originalUrl} ${statusCode} ${contentLength}-${userAgent}${ip}`
      );

      //  if (method !== 'GET') {
      //    this.logger.error(request.body);
      //  }
    });

    next();
  }
}
