import { HttpException, HttpStatus } from '@nestjs/common';

/**
 * custom exception extending from http exception
 */
export class customException extends HttpException {
  /**
   *
   * @param error message from the service
   * @param httpstatus status code
   */
  constructor(error: string, httpstatus: HttpStatus) {
    super(error, httpstatus);
  }
}
