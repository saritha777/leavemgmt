import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

/**
 * main controller
 */
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  /**
   * getHello method
   * @returns string
   */
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
