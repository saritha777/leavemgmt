import { Injectable } from '@nestjs/common';

/**
 * main service
 */
@Injectable()
export class AppService {
  /**
   * getHello method
   * @returns string
   */
  getHello(): string {
    return 'Hello World!';
  }
}
